const express = require('express');
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');
const router = express.Router();

const stuff = require('../controllers/stuff');



router.route('/:id', auth)
    .get(stuff.findOne)
    .put(multer, stuff.updateOne)
    .delete(stuff.deleteOne);

router.route('/', auth)
    .post(multer, stuff.createThing)
    .get(stuff.findAll);

module.exports = router;